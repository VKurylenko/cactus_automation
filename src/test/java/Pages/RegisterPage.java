package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RegisterPage extends BasePage {

    public RegisterPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//input[@id=\'customer_firstname\']")
    public WebElement firstName;

    @FindBy(xpath = "//input[@id='customer_lastname']")
    public WebElement lastName;

    @FindBy(xpath = "//input[@id='phone_mobile']")
    public WebElement phoneField;


    @FindBy(xpath = "//input[@id='passwd']")
    public WebElement passwordField;

    @FindBy(xpath = "//*[@id='submitAccount']")
    public WebElement registerButton;


    @Step
    public RegisterPage fillFields() {
        firstName.sendKeys("Claus");
        lastName.sendKeys("Vazovskiy");
        phoneField.click();
        phoneField.sendKeys("0685748390");
        passwordField.sendKeys("testuser");
        return this;
    }

    @Step
    public AuthPage clickButton() {
        registerButton.click();
        return new AuthPage(driver);
    }


}
