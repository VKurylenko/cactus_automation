package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Result extends BasePage {
    public Result(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[text()='Купить']")
    public WebElement zeroResults;

    @FindBy(xpath = "//p[contains(text(),'Ничего не найдено')]")
    public WebElement nothingFound;

    @FindBy(xpath = "//*[@id = 'block-categories']")
    public WebElement blockCategories;

}


