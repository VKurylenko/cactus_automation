package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[text()='Личный кабинет']")
    private WebElement personalCabinet;

    @FindBy(xpath = "//a[@title='Вход в личный кабинет']")
    private WebElement moveToLogin;

    @FindBy(xpath = "//span[text()='Купить']")
    private WebElement buyButton;

    @FindBy(xpath = "//span[@title='Продолжить покупки']")
    private WebElement continueShopping;

    @FindBy(xpath = "//div[@class='product-grid-wrapper vc-smart-special-products-grid']/div[@class='col-xs-12 col-sm-6 col-lg-3 col-md-4 no-margin product-item-holder  hover'][1]//img")
    private WebElement producOnMainPage;

    @FindBy(xpath = "//div[@class='product-grid-wrapper vc-smart-special-products-grid']/div[@class='col-xs-12 col-sm-6 col-lg-3 col-md-4 no-margin product-item-holder  hover'][1]//div[@class='compare']")
    private WebElement comparsionIcon;

    @FindBy(xpath = "//a [@class='fancybox-item fancybox-close']")
    private WebElement fancyBox;

    @FindBy(xpath = "//div[@class='product-grid-wrapper vc-smart-special-products-grid']/div[@class='col-xs-12 col-sm-6 col-lg-3 col-md-4 no-margin product-item-holder  hover'][2]//div[@class='compare']")
    private WebElement comparsionIcon2;

    @FindBy(xpath = "//a[text()='Перейти к сравнению']")
    private WebElement comparison;

    @FindBy(xpath = "//div[@style='left: -16.6667%;']")
    public WebElement sliderMoved;


    @Step
    public AuthPage getAuthPage() {
        personalCabinet.click();
        return new AuthPage(driver);
    }

    @Step
    public AuthPage clickToLogin() {
        moveToLogin.click();
        return new AuthPage(driver);

    }

    @Step
    public MainPage clickOnBuyButton() {
        buyButton.click();
        driver.findElement(By.xpath("//span[text()='Купить']"));
        return this;
    }

    @Step
    public MainPage clickOnContinueShoppingButton() {
        continueShopping.click();
        return this;
    }

    @Step
    public ProductDetails clickOnProduct() {
        producOnMainPage.click();
        return new ProductDetails(driver);

    }

    @Step
    public MainPage clickOncompassionButton() {
        comparsionIcon.click();
        return this;

    }

    @Step
    public MainPage clickOnFuncyBoxClose() {
        fancyBox.click();
        return this;
    }

    @Step
    public MainPage clickOnComparsionButton2() {
        comparsionIcon2.click();
        return this;
    }

    @Step
    public ComparsionPage moveToComparsionPage() {
        comparison.click();
        return new ComparsionPage(driver);
    }


}
