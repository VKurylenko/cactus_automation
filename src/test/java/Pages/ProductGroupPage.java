package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductGroupPage extends BasePage {

    public ProductGroupPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//img[@src='https://cactus.r.worldssl.net/c/114-medium_default/apple-watch.jpg']")
    public WebElement appleWatchGroup;

    @FindBy(xpath = "//a[@href='https://cactus.kh.ua/114-apple-watch'][@class='selected']")
    public WebElement appleWatchSideBar;

    @FindBy(xpath = "//h1 [@id='title']")
    public WebElement titleMeizuPhone;

    @FindBy(xpath = "//input [@id='layered_id_feature_34']")
    private WebElement checkBox;


    @Step
    public ProductPage checkBox() {
        checkBox.click();
        return new ProductPage(driver);
    }


}
